# Initiation of the Bot

Create an environment variables file (`.env`) and place your bot token inside with the name `TOKEN`. You can then execute the bot using:

```shell
python -m bot
```

You need to have `python-telegram-bot` installed.

