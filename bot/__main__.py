import logging
import json
import os
import sys

import dotenv
from telegram.ext import Updater
from telegram.ext import MessageHandler
from telegram.ext import Filters


def echo(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text=str(update))


if __name__ == '__main__':
    dotenv.load_dotenv()

    updater = Updater(
        token=os.environ.get('TOKEN'),
        use_context=True,
        workers=2
    )

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=int(os.environ.get('LOGGING', 20)))

    updater.dispatcher.add_handler(MessageHandler(Filters.all, echo))

    updater.start_polling()
    print('Listening...', file=sys.stderr)

    updater.idle()
    print('Terminated the bot.', file=sys.stderr)
